using System;
using UnityEngine;
using UnityEngine.UI;

namespace RepeatSequence.UI
{
    public class SequenceButton : Button
    {
        public event Action<int> SequenceButtonPressed;

        [SerializeField] private ButtonType buttonType;
        [SerializeField] private Text text;
        private int index;

        private void Start()
        {
            ButtonDown += OnButtonDown;
            ButtonUp += OnButtonUp;
            ButtonExit += OnButtonExit;
        }

        public void OnButtonDown()
        {
            ChangeTextColor(buttonType, true);
        }

        public void OnButtonUp()
        {
            ChangeTextColor(buttonType, false);
            if (!IsActive()) return;
            SequenceButtonPressed?.Invoke(index);
        }

        private void OnButtonExit()
        {
            ChangeTextColor(buttonType, false);
        }

        public ButtonType GetButtonType()
        {
            var output = buttonType;
            return output;
        }

        public int GetIndex()
        {
            int output = index;
            return output;
        }

        public void SetIndex(int value)
        {
            index = value;
        }

        public void ChangeTextColor(ButtonType buttonType, bool down)
        {
            Color normalColor = this.targetGraphic.color;
            switch (buttonType)
            {
                case ButtonType.Red:
                    if (down)
                    {
                        text.color = Color.white;
                    }
                    else
                    {
                        text.color = normalColor;
                    }
                    break;
                case ButtonType.Green:
                    if (down)
                    {
                        text.color = Color.white;
                    }
                    else
                    {
                        text.color = normalColor;
                    }
                    break;
                case ButtonType.Blue:
                    if (down)
                    {
                        text.color = Color.white;
                    }
                    else
                    {
                        text.color = normalColor;
                    }
                    break;
                case ButtonType.Yellow:
                    if (down)
                    {
                        text.color = Color.black;
                    }
                    else
                    {
                        text.color = normalColor;
                    }
                    break;
            }
        }
    }
}