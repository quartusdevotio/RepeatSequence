using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace RepeatSequence.UI
{
    public class Button : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler
    {
        [SerializeField] private bool isActive = true;
        private bool pressed = false;

        public event Action ButtonDown;
        public event Action ButtonUp;
        public event Action ButtonExit;

        public Image targetGraphic;
        public Sprite pressedSprite;
        public Sprite normalSprite;

        public virtual void OnPointerDown(PointerEventData eventData)
        {
            if (!IsActive()) return;

            targetGraphic.sprite = pressedSprite;
            pressed = true;
            ButtonDown?.Invoke();
        }

        public virtual void OnPointerUp(PointerEventData eventData)
        {
            if (!IsActive()) return;
            if (!pressed) return;

            targetGraphic.sprite = normalSprite;
            pressed = false;
            ButtonUp?.Invoke();
        }

        public virtual void OnPointerExit(PointerEventData eventData)
        {
            if (!IsActive()) return;

            targetGraphic.sprite = normalSprite;
            pressed = false;
            ButtonExit?.Invoke();
        }

        private void OnDestroy()
        {
            ButtonDown = null;
            ButtonUp = null;
            ButtonExit = null;
        }

        public bool IsActive()
        {
            bool output = isActive;
            return output;
        }

        public void Enable()
        {
            isActive = true;
        }

        public void Disable()
        {
            isActive = false;
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void Show()
        {
            gameObject.SetActive(true);
        }
    }
}