using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace RepeatSequence.Player
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private Image mascot;
        [SerializeField] private Sprite mascotDefault;
        [SerializeField] private Sprite mascotRight;
        [SerializeField] private Sprite[] mascotWrong;
        [SerializeField] private Sprite health;
        [SerializeField] private Sprite emptyHealth;
        [SerializeField] private Image[] healthIndicators;
        [SerializeField] private GameObject dialogueBox;
        [SerializeField] private TMP_Text dialogueText;
        [SerializeField] private string[] rightDialoguePool;
        [SerializeField] private string[] wrongDialoguePool;
        private int life;
        private const int maxLife = 3;

        public event Action Dead;

        private void Start()
        {
            life = maxLife;
        }

        public void ShowDialogue(bool correct)
        {
            dialogueBox.SetActive(true);
            if(correct)
            {
                mascot.sprite = mascotRight;
                dialogueText.text = rightDialoguePool[Random.Range(0, rightDialoguePool.Length)];
            }
            else
            {
                mascot.sprite = mascotWrong[Random.Range(0, 2)];
                dialogueText.text = wrongDialoguePool[Random.Range(0, wrongDialoguePool.Length)];
                SetHealth(life - 1);
            }
        }

        private void HideDialogue()
        {
            dialogueBox.SetActive(false);
        }

        private void SetHealth(int value)
        {
            life = value;
            for(int i=3; i > life; i--)
            {
                healthIndicators[life].sprite = emptyHealth;
            }

            if (value <= 0)
            {
                Dead?.Invoke();
            }
        }

        public int GetHealth()
        {
            int output = life;
            return output;
        }

        public void ResetMascot()
        {
            mascot.sprite = mascotDefault;
            HideDialogue();
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Reset()
        {
            SetHealth(maxLife);
            for (int i = 0; i < life; i++)
            {
                healthIndicators[i].sprite = health;
            }
            ResetMascot();
        }
    }
}