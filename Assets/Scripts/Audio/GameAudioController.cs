using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RepeatSequence.Manager
{
    public class GameAudioController : MonoBehaviour
    {
        [SerializeField] private GameManager gameManager;
        [SerializeField] private Sound[] soundClips;
        [SerializeField] private AudioSource soundSource;

        private void Start()
        {
            gameManager.SequencePressed += OnSequencePressed;
            gameManager.SequenceFinished += OnSequenceFinished;
            gameManager.GameOver += OnGameOver;
        }

        private void OnDestroy()
        {
            gameManager.SequencePressed -= OnSequencePressed;
            gameManager.SequenceFinished -= OnSequenceFinished;
            gameManager.GameOver -= OnGameOver;
        }

        private void OnSequencePressed(bool correct)
        {
            if(correct)
            {
                PlaySound("Answer_Right");
            }
            else
            {
                PlaySound("Answer_Wrong");
            }
            
        }

        private void OnSequenceFinished()
        {
            PlaySound("Sequence_Right");
        }

        private void OnGameOver(bool won)
        {
            if(won)
            {
                PlaySound("Final_Win");
            }
            else
            {
                PlaySound("Final_Lose");
            }
        }

        private void PlaySound(string name)
        {
            Sound sound = Array.Find(soundClips, x => x.name == name);

            if(sound != null)
            {
                soundSource.clip = sound.clip;
                soundSource.PlayOneShot(soundSource.clip);
            }
        }
    }
}