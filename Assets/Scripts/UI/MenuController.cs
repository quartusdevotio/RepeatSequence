﻿using UnityEngine;

namespace RepeatSequence.UI
{
    public abstract class MenuController : MonoBehaviour
    {
        [SerializeField] protected Canvas canvas;

        public void Hide()
        {
            canvas.enabled = false;
        }

        public void Show()
        {
            canvas.enabled = true;
        }
    }
}