public interface IPlay
{
    public void OnPlayButtonUp();
}

public interface IContinue
{
    public void OnContinueButtonUp();
}

public interface IRestart
{
    public void OnRestartButtonUp();
}

public interface IQuit
{
    public void OnQuitButtonUp();
}