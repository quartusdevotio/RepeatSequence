using System;
using UnityEngine;

namespace RepeatSequence.UI
{
    public class GameOverPopupController : PopupController, IRestart, IQuit
    {
        public event Action PlayAgainPressed;
        public event Action QuitPressed;

        [SerializeField] private Button playAgainButton;
        [SerializeField] private Button quitButton;

        void Start()
        {
            playAgainButton.ButtonUp += OnRestartButtonUp;
            quitButton.ButtonUp += OnQuitButtonUp;
        }

        void OnDestroy()
        {
            playAgainButton.ButtonUp -= OnRestartButtonUp;
            quitButton.ButtonUp -= OnQuitButtonUp;
        }

        public void OnRestartButtonUp()
        {
            PlayAgainPressed?.Invoke();
        }

        public void OnQuitButtonUp()
        {
            QuitPressed?.Invoke();
        }
    }
}