using System;
using UnityEngine;

namespace RepeatSequence.UI
{
    public class PausePopupController : PopupController, IContinue, IRestart, IQuit
    {
        public event Action ContinuePressed;
        public event Action RestartPressed;
        public event Action QuitPressed;

        [SerializeField] private Button continueButton;
        [SerializeField] private Button restartButton;
        [SerializeField] private Button quitButton;

        void Start()
        {
            continueButton.ButtonUp += OnContinueButtonUp;
            restartButton.ButtonUp += OnRestartButtonUp;
            quitButton.ButtonUp += OnQuitButtonUp;
        }

        private void OnDestroy()
        {
            continueButton.ButtonUp -= OnContinueButtonUp;
            restartButton.ButtonUp -= OnRestartButtonUp;
            quitButton.ButtonUp -= OnQuitButtonUp;
        }

        public void OnContinueButtonUp()
        {
            ContinuePressed?.Invoke();
        }

        public void OnRestartButtonUp()
        {
            RestartPressed?.Invoke();
        }

        public void OnQuitButtonUp()
        {
            QuitPressed?.Invoke();
        }
    }
}