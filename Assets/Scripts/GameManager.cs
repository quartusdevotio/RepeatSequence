using RepeatSequence.Player;
using RepeatSequence.UI;
using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace RepeatSequence.Manager
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private StartMenuController startMenu;
        [SerializeField] private PausePopupController pausePopup;
        [SerializeField] private GameOverPopupController gameOverPopup;
        [SerializeField] private Sequencer sequencer;
        [SerializeField] private PlayerController playerController;

        [SerializeField] private GameMode gameMode = GameMode.None;

        [SerializeField] private List<int> randomSequence = new List<int>();
        [SerializeField] private int totalSequence;
        [SerializeField] private int maxSequence;
        private int totalButtons;
        private int currentSequence = 0;

        public event Action<bool> SequencePressed;
        public event Action SequenceFinished;
        public event Action<int, List<int>> NextSequencePrepared;
        public event Action<bool> GameOver;

        private bool isPaused = false;

        private bool won = false;

        private void Start()
        {
            Initialize();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                OnPausePressed();
            }
        }

        private void Initialize()
        {
            totalButtons = sequencer.sequenceButtons.Count;
            sequencer.ButtonPressed += OnButtonPressed;
            startMenu.NormalModePressed += OnNormalModePressed;
            startMenu.HardModePressed += OnHardModePressed;
            startMenu.QuitPressed += OnQuitPressed;
            gameOverPopup.PlayAgainPressed += OnRestartPressed;
            gameOverPopup.QuitPressed += OnQuitPressed;
            pausePopup.ContinuePressed += OnPausePressed;
            pausePopup.RestartPressed += OnRestartPressed;
            pausePopup.QuitPressed += OnQuitPressed;
            NextSequencePrepared += sequencer.OnSequenceFinished;
            SequencePressed += OnSequencePressed;
            playerController.Dead += OnPlayerDead;
        }

        private void OnDestroy()
        {
            sequencer.ButtonPressed -= OnButtonPressed;
            startMenu.NormalModePressed -= OnNormalModePressed;
            startMenu.HardModePressed -= OnHardModePressed;
            startMenu.QuitPressed -= OnQuitPressed;
            gameOverPopup.PlayAgainPressed -= OnRestartPressed;
            gameOverPopup.QuitPressed -= OnQuitPressed;
            pausePopup.ContinuePressed -= OnPausePressed;
            pausePopup.RestartPressed -= OnRestartPressed;
            pausePopup.QuitPressed -= OnQuitPressed;
            NextSequencePrepared -= sequencer.OnSequenceFinished;
            SequencePressed -= OnSequencePressed;
            playerController.Dead -= OnPlayerDead;
        }

        private void StartGame()
        {
            sequencer.Show();
            foreach(Button button in sequencer.sequenceButtons)
            {
                button.Show();
                button.Enable();
            }
            ShowNextSequence();
        }

        private void ShowNextSequence()
        {
            playerController.ResetMascot();
            RandomizeSequence();
            NextSequencePrepared?.Invoke(totalSequence, randomSequence);
        }

        private void ShowSequence()
        {
            playerController.ResetMascot();
            NextSequencePrepared?.Invoke(totalSequence, randomSequence);
        }

        private void RandomizeSequence()
        {
            currentSequence = 0;
            if(gameMode == GameMode.Hard)
            {
                randomSequence.Clear();
                for(int i = 0; i < totalSequence; i++)
                {
                    randomSequence.Add(Random.Range(0, totalButtons));
                }
            }
            else if(gameMode == GameMode.Normal)
            {
                while(randomSequence.Count < totalSequence)
                {
                    randomSequence.Add(Random.Range(0, totalButtons));
                }
            }
        }

        private void OnPausePressed()
        {
            if (isPaused)
            {
                Time.timeScale = 1f;
                isPaused = false;
                pausePopup.Hide();
            }
            else
            {
                Time.timeScale = 0f;
                isPaused = true;
                pausePopup.Show();
            }
        }

        private void OnRestartPressed()
        {
            totalSequence = 3;
            sequencer.StopSimulation();
            playerController.Reset();
            gameOverPopup.Hide();
            StartGame();
        }

        private void OnQuitPressed()
        {
            Application.Quit();
        }

        private void OnPlayerDead()
        {
            foreach(Button button in sequencer.sequenceButtons)
            {
                button.Disable();
            }
            gameOverPopup.Show();
            GameOver?.Invoke(won);
        }

        private void OnButtonPressed(ButtonType buttonType)
        {
            ValidateButtonPress(buttonType);
        }

        private void OnSequencePressed(bool correct)
        {
            playerController.ShowDialogue(correct);
        }

        private void ValidateButtonPress(ButtonType buttonType)
        {
            if(currentSequence == totalSequence-1 && (int)buttonType == randomSequence[currentSequence])
            {
                if(totalSequence == maxSequence)
                {
                    won = true;
                    OnPlayerDead();
                    return;
                }

                totalSequence++;
                ShowNextSequence();
                return;
            }

            if((int)buttonType == randomSequence[currentSequence])
            {
                currentSequence++;
                SequencePressed?.Invoke(true);
            }
            else
            {
                if(gameMode == GameMode.Normal && playerController.GetHealth() > 1)
                {
                    ShowSequence();
                }
                else if(gameMode == GameMode.Hard && playerController.GetHealth() > 1)
                {
                    ShowNextSequence();
                }
                currentSequence = 0;
                SequencePressed?.Invoke(false);
            }
        }

        private void OnNormalModePressed()
        {
            gameMode = GameMode.Normal;
            StartGame();
        }

        private void OnHardModePressed()
        {
            gameMode = GameMode.Hard;
            StartGame();
        }

        
    }
}