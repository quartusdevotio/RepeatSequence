using System;
using UnityEngine;

namespace RepeatSequence.UI
{
    public class StartMenuController : MenuController, IPlay, IQuit
    {
        public event Action PlayPressed;
        public event Action QuitPressed;
        public event Action NormalModePressed;
        public event Action HardModePressed;

        [SerializeField] private Button playButton;
        [SerializeField] private Button quitButton;
        [SerializeField] private Button normalModeButton;
        [SerializeField] private Button hardModeButton;

        private void Start()
        {
            playButton.ButtonUp += OnPlayButtonUp;
            quitButton.ButtonUp += OnQuitButtonUp;
            normalModeButton.ButtonUp += OnNormalModeButtonUp;
            hardModeButton.ButtonUp += OnHardModeButtonUp;
        }

        private void OnDestroy()
        {
            playButton.ButtonUp -= OnPlayButtonUp;
            quitButton.ButtonUp -= OnQuitButtonUp;
            normalModeButton.ButtonUp -= OnNormalModeButtonUp;
            hardModeButton.ButtonUp -= OnHardModeButtonUp;
        }

        public void OnPlayButtonUp()
        {
            HidePlayMenu();
            ShowModeMenu();
            PlayPressed?.Invoke();
        }

        public void OnQuitButtonUp()
        {
            Application.Quit();
            QuitPressed?.Invoke();
        }

        private void OnNormalModeButtonUp()
        {
            HideModeMenu();
            Hide();
            NormalModePressed?.Invoke();
        }

        private void OnHardModeButtonUp()
        {
            HideModeMenu();
            Hide();
            HardModePressed?.Invoke();
        }

        private void HidePlayMenu()
        {
            playButton.gameObject.SetActive(false);
            quitButton.gameObject.SetActive(false);
        }

        private void ShowPlayMenu()
        {
            playButton.gameObject.SetActive(true);
            quitButton.gameObject.SetActive(true);
        }

        private void HideModeMenu()
        {
            normalModeButton.gameObject.SetActive(false);
            hardModeButton.gameObject.SetActive(false);
        }

        private void ShowModeMenu()
        {
            normalModeButton.gameObject.SetActive(true);
            hardModeButton.gameObject.SetActive(true);
        }
    }
}