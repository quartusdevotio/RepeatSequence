using RepeatSequence.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RepeatSequence.Manager
{
    public class Sequencer : MonoBehaviour
    {
        public List<SequenceButton> sequenceButtons = new List<SequenceButton>();
        public event Action<ButtonType> ButtonPressed;

        private Coroutine simulationCoroutine;

        [SerializeField] private Canvas gameCanvas;


        void Start()
        {
            for(int i=0; i<sequenceButtons.Count; i++)
            {
                int buttonIndex = i;
                sequenceButtons[i].SetIndex(i);
                sequenceButtons[i].SequenceButtonPressed += OnButtonPress;
            }
        }

        private void OnDestroy()
        {
            foreach(SequenceButton sequenceButton in sequenceButtons)
            {
                sequenceButton.SequenceButtonPressed -= OnButtonPress;
            }
        }

        public IEnumerator SimulateSequence(int totalSequence, List<int> randomSequence)
        {
            foreach (Button button in sequenceButtons)
            {
                button.Disable();
            }
            for (int i = 0; i < totalSequence; i++)
            {
                var button = sequenceButtons[randomSequence[i]];
                button.targetGraphic.sprite = button.pressedSprite;
                button.OnButtonDown();
                yield return new WaitForSeconds(2.5f / totalSequence);
                button.targetGraphic.sprite = button.normalSprite;
                button.OnButtonUp();
                yield return new WaitForSeconds(0.5f / totalSequence);
            }
            foreach (Button button in sequenceButtons)
            {
                button.Enable();
            }
        }

        private void OnButtonPress(int buttonIndex)
        {
            ButtonPressed?.Invoke((ButtonType)buttonIndex);
        }

        public void OnSequenceFinished(int totalSequence, List<int> randomSequence)
        {
            simulationCoroutine = StartCoroutine(SimulateSequence(totalSequence, randomSequence));
        }

        public void StopSimulation()
        {
            StopCoroutine(simulationCoroutine);
        }

        public void Hide()
        {
            gameCanvas.enabled = false;
        }

        public void Show()
        {
            gameCanvas.enabled = true;
        }
    }
}